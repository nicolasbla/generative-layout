<?php
require_once __DIR__ . '/bootstrap/start.php';

mt_srand();
$seed = mt_rand();

$generator = new \GenerativeLayout\LayoutGenerator($seed, 0, 5, 7);
for ($i = 0; $i < 60; $i++) {
	$generator->step();
}

$renderer = new \GenerativeLayout\LayoutRenderer($generator->getLayout());

$content = $renderer->render();

include __DIR__ . '/app/views/index.php';