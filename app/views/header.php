<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Generative Layout</title>

		<link href="assets/generative-layout/stylesheets/screen.css" media="print, screen, projection" rel="stylesheet" type="text/css" />
		
		<link href="assets/generative-layout/stylesheets/print.css" media="print" rel="stylesheet" type="text/css" />

		<!--[if (gte IE 6)&(lte IE 8)]>
			<script type="text/javascript" src="selectivizr.js"></script>
		<![endif]--> 

		<!--[if IE]>
		  <link href="assets/generative-layout/stylesheets/ie.css" media="screen, projection" rel="stylesheet" type="text/css" />
		<![endif]-->

  		<script src="assets/modernizr/custom.modernizr.js"></script>
    </head>
    <body>
    	<header>
    		<h1>Generative Layout (Seed: <?php echo $seed; ?>)</h1>
    	</header>