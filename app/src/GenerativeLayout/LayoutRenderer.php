<?php
namespace GenerativeLayout;

class LayoutRenderer {
	protected $layout;

	function __construct($layout) {
		$this->layout = $layout;
	}

	function render() {
		ob_start();
		foreach ($this->layout as $node) {
			$this->renderNode($node, true);
		}
		return ob_get_clean();
	}

	function renderNode($node, $isRoot = false) {
		if ( sizeof($node['children']) ) {
			?>
<div class="<?php echo $node['type']; ?> count-<?php echo sizeof($node['children']); ?> <?php echo ($isRoot ? 'is-root': ''); ?>"> 
	<?php foreach ($node['children'] as $child) {
		$this->renderNode($child);
	} ?> 
</div>
			<?php
		} else { 
			?> 
<div class="<?php echo $node['type']; ?> empty <?php echo ($isRoot ? 'is-root': ''); ?>"><div></div></div> 
			<?php
		}
	}
}