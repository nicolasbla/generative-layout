<?php
namespace GenerativeLayout;

class LayoutGenerator {
	protected $layout;
	protected $nodeQueue;
	protected $seed;
	protected $maxDepth;
	protected $maxRowChildCount;
	protected $maxColumnChildCount;

	function __construct($seed = false, $maxDepth = 0, $maxRowChildCount = 0, $maxColumnChildCount = 0) {
		$this->seed = $seed;
		$this->maxDepth = $maxDepth;
		$this->maxRowChildCount = $maxRowChildCount;
		$this->maxColumnChildCount = $maxColumnChildCount;
		$this->reset();
	}

	function getLayout() {
		return $this->layout;
	}

	function reset() {
		$this->layout = array();
		$this->queue = array();
		if ($this->seed) {
			mt_srand($this->seed);	
		} else {
			mt_srand();	
		}
	}

	function step() {
		if ( ! sizeof( $this->queue ) ) {
			if ( mt_rand(0, 1) ) {
				$childNode = $this->createRowNode();
			} else {
				$childNode = $this->createColumnNode();
			}
			$this->layout[] = &$childNode;
			$this->queue[] = &$childNode;
		} else {
			$index = mt_rand(0, sizeof($this->queue) - 1);
			$node = & $this->queue[$index];
			array_splice($this->queue[$index], $index, 0);

			$incrementDepth = false;

			if ($node['type'] == 'row') {
				if ($this->maxColumnChildCount < 1 || sizeof($node['children']) < $this->maxColumnChildCount) {

					if ( 'row' == $node['type'] ) {
						$childNode = $this->createColumnNode();
					} else {
						$childNode = $this->createRowNode();
					}

					$childNode['parent'] =& $node;
					$node['children'][] =& $childNode;

					$this->queue[] =& $childNode;
					$incrementDepth = true;
				}
			} else {
				if ($this->maxRowChildCount < 1 || sizeof($node['children']) < $this->maxRowChildCount) {

					if ( 'row' == $node['type'] ) {
						$childNode = $this->createColumnNode();
					} else {
						$childNode = $this->createRowNode();
					}

					$childNode['parent'] =& $node;
					$node['children'][] =& $childNode;

					$this->queue[] =& $childNode;
					$incrementDepth = true;
				}
			}

			if ($incrementDepth) {
				$this->incrementParentDepth($node);
			}
		}
	}

	function incrementParentDepth( & $node ) {
		$node['depth']++;
		if ($node['parent']) {
			$this->incrementParentDepth($node['parent']);
		} else if ( $this->maxDepth > 0 && $node['depth'] >= $this->maxDepth ) {
			$this->removeBranchFromQueue( $node );
		}
	}

	function removeBranchFromQueue( & $node ) {
		$i = 0;
		$size = sizeof($this->queue);
		while ( $i < $size ) {
			$queueNode =& $this->queue[$i];
			$node['**ref**'] = true;
			if ( isset($queueNode['**ref**']) ) {
				array_splice($this->queue, $i, 0);
				$size--;
				//echo "{$queueNode['nodeId']}, {$queueNode['type']}<br>";
			} else {
				$i++;
			}
			unset($node['**ref**']);
		}
		unset($queueNode);

		if ( sizeof($node['children']) ) {
			foreach ($node['children'] as & $child) {
				$this->removeBranchFromQueue($child);
			}
			unset($child);
		}
	}

	function createRowNode() {
		return array('nodeId' => sha1(time()), 'type' => 'row', 'children' => array(), 'depth' => 0, 'parent' => null);
	}

	function createColumnNode() {
		return array('nodeId' => sha1(time()), 'type' => 'column', 'children' => array(), 'depth' => 0, 'parent' => null);
	}
}